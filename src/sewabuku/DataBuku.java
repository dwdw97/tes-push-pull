package sewabuku;

import java.awt.EventQueue;


import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;

public class DataBuku {

	public JFrame formDataBuku;
	private JTextField txtJudul;
	private JTextField txtPengarang;
	private JTextField txtPenerbit;
	private JTextField txtTahunTerbit;
	private JTable tableBuku;
	private JComboBox boxStatus;
	private JLabel lblIdBuku;

	// java-mysql connect
	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://127.0.0.1/sewabuku";
	static final String USER = "root";
	static final String PASS = "";
	
	static Connection conn;
	static Statement stmt;
	static ResultSet rs;
	private DefaultTableModel model;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DataBuku window = new DataBuku();
					window.formDataBuku.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public DataBuku() {
		initialize();
	}

	private void initialize() {
		formDataBuku = new JFrame();
		formDataBuku.setFont(new Font("Tahoma", Font.PLAIN, 12));
		formDataBuku.setResizable(false);
		formDataBuku.setTitle("Data Buku");
		formDataBuku.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				showData();
			}
		});
		formDataBuku.setBounds(100, 100, 618, 470);
		formDataBuku.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		formDataBuku.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Data Buku");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 14));
		lblNewLabel.setBounds(10, 11, 88, 28);
		formDataBuku.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Judul Buku");
		lblNewLabel_1.setFont(new Font("Arial", Font.PLAIN, 12));
		lblNewLabel_1.setBounds(10, 50, 88, 14);
		formDataBuku.getContentPane().add(lblNewLabel_1);
		
		txtJudul = new JTextField();
		txtJudul.setBounds(108, 47, 159, 20);
		formDataBuku.getContentPane().add(txtJudul);
		txtJudul.setColumns(10);
		
		JLabel lblNewLabel_1_1 = new JLabel("Pengarang");
		lblNewLabel_1_1.setFont(new Font("Arial", Font.PLAIN, 12));
		lblNewLabel_1_1.setBounds(10, 78, 88, 14);
		formDataBuku.getContentPane().add(lblNewLabel_1_1);
		
		txtPengarang = new JTextField();
		txtPengarang.setColumns(10);
		txtPengarang.setBounds(108, 75, 159, 20);
		formDataBuku.getContentPane().add(txtPengarang);
		
		JLabel lblNewLabel_1_2 = new JLabel("Penerbit");
		lblNewLabel_1_2.setFont(new Font("Arial", Font.PLAIN, 12));
		lblNewLabel_1_2.setBounds(10, 106, 88, 14);
		formDataBuku.getContentPane().add(lblNewLabel_1_2);
		
		txtPenerbit = new JTextField();
		txtPenerbit.setColumns(10);
		txtPenerbit.setBounds(108, 103, 159, 20);
		formDataBuku.getContentPane().add(txtPenerbit);
		
		JLabel lblNewLabel_1_3 = new JLabel("Tahun Terbit");
		lblNewLabel_1_3.setFont(new Font("Arial", Font.PLAIN, 12));
		lblNewLabel_1_3.setBounds(10, 134, 88, 14);
		formDataBuku.getContentPane().add(lblNewLabel_1_3);
		
		txtTahunTerbit = new JTextField();
		txtTahunTerbit.setColumns(10);
		txtTahunTerbit.setBounds(108, 131, 95, 20);
		formDataBuku.getContentPane().add(txtTahunTerbit);
		
		JLabel lblNewLabel_1_3_1 = new JLabel("Status");
		lblNewLabel_1_3_1.setFont(new Font("Arial", Font.PLAIN, 12));
		lblNewLabel_1_3_1.setBounds(10, 159, 88, 14);
		formDataBuku.getContentPane().add(lblNewLabel_1_3_1);
		
		
		boxStatus = new JComboBox();
		boxStatus.setFont(new Font("Arial", Font.PLAIN, 11));
		boxStatus.setModel(new DefaultComboBoxModel(new String[] {"Tersedia", "Tidak Tersedia"}));
		boxStatus.setBounds(108, 155, 95, 22);
		formDataBuku.getContentPane().add(boxStatus);
		
		JButton btnSimpan = new JButton("Simpan");
		btnSimpan.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnSimpan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String judul = txtJudul.getText();
				String pengarang = txtPengarang.getText();
				String penerbit = txtPenerbit.getText();
				int tahun_terbit = Integer.parseInt(txtTahunTerbit.getText());
				Object status = boxStatus.getSelectedItem();
				
				simpanData(judul, pengarang, penerbit, tahun_terbit, status);
			}
		});
		btnSimpan.setBounds(10, 186, 89, 23);
		formDataBuku.getContentPane().add(btnSimpan);
		
		JButton btnEdit = new JButton("Edit");
		btnEdit.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnEdit.setEnabled(false);
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id = Integer.parseInt(lblIdBuku.getText());
				ubahData(id);
			}
		});
		btnEdit.setBounds(108, 186, 89, 23);
		formDataBuku.getContentPane().add(btnEdit);
		
		JButton btnHapus = new JButton("Hapus");
		btnHapus.setEnabled(false);
		btnHapus.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnHapus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int response = JOptionPane.showConfirmDialog(null, "Yakin ingin menghapus?");
				if (response==0) {
					if(tableBuku.getSelectedRow()>0) {
						hapusData(Integer.parseInt(lblIdBuku.getText()));
					}
				} else {
					JOptionPane.showMessageDialog(null, "Hapus data dibatalkan");
				}
			}
		});
		btnHapus.setBounds(207, 186, 89, 23);
		formDataBuku.getContentPane().add(btnHapus);
		
		JButton btnCetakLaporan = new JButton("Cetak Laporan");
		btnCetakLaporan.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnCetakLaporan.setBounds(459, 186, 133, 23);
		formDataBuku.getContentPane().add(btnCetakLaporan);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 220, 582, 178);
		formDataBuku.getContentPane().add(scrollPane);
		
		tableBuku = new JTable();
		tableBuku.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String judul = tableBuku.getValueAt(tableBuku.getSelectedRow(), 0).toString();
				String pengarang = tableBuku.getValueAt(tableBuku.getSelectedRow(), 1).toString();
				String penerbit = tableBuku.getValueAt(tableBuku.getSelectedRow(), 2).toString();
				getData(judul, pengarang, penerbit);
				btnSimpan.setEnabled(false);
				btnEdit.setEnabled(true);
			}
		});
		scrollPane.setViewportView(tableBuku);
		
		lblIdBuku = new JLabel("");
		lblIdBuku.setEnabled(false);
		lblIdBuku.setBounds(108, 19, 46, 14);
		lblIdBuku.setVisible(false);
		formDataBuku.getContentPane().add(lblIdBuku);
		
		JButton btnReset = new JButton("Reset");
		btnReset.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtJudul.setText("");
				txtPengarang.setText("");
				txtPenerbit.setText("");
				txtTahunTerbit.setText("");
				boxStatus.setSelectedIndex(0);
				btnSimpan.setEnabled(true);
				btnEdit.setEnabled(false);
			}
		});
		btnReset.setBounds(306, 186, 89, 23);
		formDataBuku.getContentPane().add(btnReset);
		
		JMenuBar menuBarBuku = new JMenuBar();
		formDataBuku.setJMenuBar(menuBarBuku);
		
		JMenu menuDataSewa = new JMenu("Data Sewa");
		menuDataSewa.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		menuBarBuku.add(menuDataSewa);
	}
	
	public String statusToText(String status) {
		if(status.equals("0")) {
			return "Tidak Tersedia";
		} else {
			return "Tersedia";
		}
	}
	
	public String statusToBinary(Object status) {
		if(status.equals("Tidak Tersedia")) {
			return "0";
		} else {
			return "1";
		}
	}
	
	public void showData() {
		model = new DefaultTableModel();
		
		model.addColumn("Judul");
		model.addColumn("Pengarang");
		model.addColumn("Penerbit");
		model.addColumn("Tahun Terbit");
		model.addColumn("Status");
		
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery("SELECT * FROM buku");
			while(rs.next()) {
				model.addRow(new Object[] {
					rs.getString("judul_buku"),
					rs.getString("pengarang"),
					rs.getString("penerbit"),
					rs.getString("tahun_terbit"),
					statusToText(rs.getString("status")),
				});
			}
			
			stmt.close();
			conn.close();
			
			tableBuku.setModel(model);
			tableBuku.setAutoResizeMode(1);
		} catch(Exception e) {
			System.out.println("Koneksi gagal");
			System.out.println(e);
		}
	}
	
	public void getData(String judul, String pengarang, String penerbit) {
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			
			String query = "SELECT * FROM buku WHERE (judul_buku = ? AND pengarang = ? AND penerbit = ?)";
			PreparedStatement ps = conn.prepareStatement(query);
			
			ps.setString(1, judul);
			ps.setString(2, pengarang);
			ps.setString(3, penerbit);
			
			rs = ps.executeQuery();
			
			rs.next();
			
			lblIdBuku.setText(Integer.toString(rs.getInt("buku_id")));
			txtJudul.setText(rs.getString("judul_buku"));
			txtPengarang.setText(rs.getString("pengarang"));
			txtPenerbit.setText(rs.getString("penerbit"));
			txtTahunTerbit.setText(rs.getString("tahun_terbit"));
			boxStatus.setSelectedItem(statusToText(rs.getString("status")));
			
			stmt.close();
			conn.close();
		} catch(Exception e) {
			System.out.println("Koneksi gagal");
			System.out.println(e);
		}
	}
	
	public void simpanData(String judul, String pengarang, String penerbit, int tahun_terbit, Object status) {
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			
			String query = "INSERT INTO buku (judul_buku, pengarang, penerbit, tahun_terbit, status) VALUES (?,?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(query);
			
			ps.setString(1, judul);
			ps.setString(2, pengarang);
			ps.setString(3, penerbit);
			ps.setInt(4, tahun_terbit);
			ps.setString(5, statusToBinary(status));
			
			ps.execute();
			
			stmt.close();
			conn.close();			
		} catch(Exception e) {
			System.out.println("Koneksi gagal");
			System.out.println(e);
		}
		
		showData();
	}
	
	public void ubahData(int id) {
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			
			String query = "UPDATE buku SET judul_buku = ?, pengarang = ?, penerbit = ?, tahun_terbit = ?, status = ? WHERE buku_id=?";
			PreparedStatement ps = conn.prepareStatement(query);
			
			ps.setString(1, txtJudul.getText());
			ps.setString(2, txtPengarang.getText());
			ps.setString(3, txtPenerbit.getText());
			ps.setInt(4, Integer.parseInt(txtTahunTerbit.getText()));
			ps.setString(5, statusToBinary(boxStatus.getSelectedItem()));
			ps.setInt(6, id);
			
			ps.execute();
			
			stmt.close();
			conn.close();			
		} catch(Exception e) {
			System.out.println("Koneksi gagal");
			System.out.println(e);
		}
		
		showData();
	}
	
	public void hapusData(int id) {
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			
			String query = "DELETE FROM buku WHERE buku_id = ?";
			PreparedStatement ps = conn.prepareStatement(query);
			
			ps.setInt(1, id);
			
			ps.execute();
			
			stmt.close();
			conn.close();
		} catch(Exception e) {
			System.out.println("Koneksi gagal");
			System.out.println(e);
		}
		showData();
	}
}
