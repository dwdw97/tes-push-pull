package sewabuku;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JTable;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DataSewa {

	private JFrame formDataSewa;
	private JTextField txtPeminjam;
	private JTable tableSewa;
	private JComboBox boxJudulBuku;
	
	// java-mysql connect
	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://127.0.0.1/sewabuku";
	static final String USER = "root";
	static final String PASS = "";
		
	static Connection conn;
	static Statement stmt;
	static ResultSet rs;
	private DefaultTableModel model;	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DataSewa window = new DataSewa();
					window.formDataSewa.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public DataSewa() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		formDataSewa = new JFrame();
		formDataSewa.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				showData();
			}
		});
		formDataSewa.setBounds(100, 100, 582, 470);
		formDataSewa.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		formDataSewa.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Persewaan Buku XYZ");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(10, 11, 546, 14);
		formDataSewa.getContentPane().add(lblNewLabel);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBackground(Color.WHITE);
		panel.setBounds(10, 36, 546, 29);
		formDataSewa.getContentPane().add(panel);
		
		JLabel lblNewLabel_1 = new JLabel("Tanggal:");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_1.setBounds(10, 0, 50, 29);
		panel.add(lblNewLabel_1);
		
		JLabel lblTanggal = new JLabel("");
		lblTanggal.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblTanggal.setBounds(62, 0, 117, 29);
		panel.add(lblTanggal);
		
		JLabel lblNewLabel_1_1 = new JLabel("Jam:");
		lblNewLabel_1_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel_1_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_1_1.setBounds(451, 0, 30, 29);
		panel.add(lblNewLabel_1_1);
		
		JLabel lblJam = new JLabel("");
		lblJam.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblJam.setBounds(480, 0, 56, 29);
		panel.add(lblJam);
		
		JLabel lblNewLabel_2 = new JLabel("Nama Peminjam");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_2.setBounds(32, 94, 75, 14);
		formDataSewa.getContentPane().add(lblNewLabel_2);
		
		txtPeminjam = new JTextField();
		txtPeminjam.setColumns(10);
		txtPeminjam.setBounds(117, 91, 120, 20);
		formDataSewa.getContentPane().add(txtPeminjam);
		
		JLabel lblNewLabel_2_1 = new JLabel("Judul Buku");
		lblNewLabel_2_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_2_1.setBounds(32, 122, 75, 14);
		formDataSewa.getContentPane().add(lblNewLabel_2_1);
		
		JLabel lblNewLabel_3 = new JLabel("Biaya      Rp");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_3.setBounds(258, 106, 70, 14);
		formDataSewa.getContentPane().add(lblNewLabel_3);
		
		JLabel lblBiaya = new JLabel("");
		lblBiaya.setVerticalAlignment(SwingConstants.BOTTOM);
		lblBiaya.setHorizontalAlignment(SwingConstants.LEFT);
		lblBiaya.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblBiaya.setBounds(328, 97, 120, 27);
		formDataSewa.getContentPane().add(lblBiaya);
		
		JButton btnSimpan = new JButton("Simpan");
		btnSimpan.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnSimpan.setBounds(10, 163, 89, 23);
		formDataSewa.getContentPane().add(btnSimpan);
		
		JButton btnEdit = new JButton("Edit");
		btnEdit.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnEdit.setEnabled(false);
		btnEdit.setBounds(109, 163, 61, 23);
		formDataSewa.getContentPane().add(btnEdit);
		
		JButton btnKembalikanBuku = new JButton("Kembalikan Buku");
		btnKembalikanBuku.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnKembalikanBuku.setEnabled(false);
		btnKembalikanBuku.setBounds(258, 163, 120, 23);
		formDataSewa.getContentPane().add(btnKembalikanBuku);
		
		JButton btnCetakLaporan = new JButton("Cetak Laporan");
		btnCetakLaporan.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnCetakLaporan.setBounds(442, 163, 114, 23);
		formDataSewa.getContentPane().add(btnCetakLaporan);
		
		boxJudulBuku = new JComboBox();
		boxJudulBuku.setFont(new Font("Tahoma", Font.PLAIN, 11));
		boxJudulBuku.setBounds(117, 118, 120, 22);
		formDataSewa.getContentPane().add(boxJudulBuku);
		
		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtPeminjam.setText("");
				txtPeminjam.setEnabled(true);
				boxJudulBuku.setEnabled(true);
				btnSimpan.setEnabled(true);
				btnEdit.setEnabled(false);
				btnKembalikanBuku.setEnabled(false);
				lblBiaya.setText("");
				tableSewa.clearSelection();
			}
		});
		btnReset.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnReset.setBounds(180, 163, 66, 23);
		formDataSewa.getContentPane().add(btnReset);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 197, 546, 201);
		formDataSewa.getContentPane().add(scrollPane);
		
		tableSewa = new JTable();
		tableSewa.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int id = Integer.parseInt(tableSewa.getValueAt(tableSewa.getSelectedRow(), 0).toString());
				String judul = tableSewa.getValueAt(tableSewa.getSelectedRow(), 2).toString();
				getData(id, judul);
				btnSimpan.setEnabled(false);
				btnEdit.setEnabled(true);
				btnKembalikanBuku.setEnabled(true);
			}
		});
		scrollPane.setColumnHeaderView(tableSewa);
		
		JMenuBar menuBarSewa = new JMenuBar();
		formDataSewa.setJMenuBar(menuBarSewa);
		
		JMenu menuDataBuku = new JMenu("Data Buku");
		menuDataBuku.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		menuBarSewa.add(menuDataBuku);
	}
	
	public void showData() {
		model = new DefaultTableModel();
		
		model.addColumn("No");
		model.addColumn("Peminjam");
		model.addColumn("Judul Buku");
		model.addColumn("Tanggal Pinjam");
		model.addColumn("Tanggal Dikembalikan");
		model.addColumn("Denda");
		model.addColumn("Biaya Total");
		
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery("SELECT id, nama_peminjam, buku.judul_buku, date_format(tanggal_pinjam, '%d-%m-%Y'), date_format(tanggal_kembali, '%d-%m-%Y'), denda, biaya_sewa, buku.buku_id, buku.status FROM `sewabuku` INNER JOIN buku ON sewabuku.`buku_id`=buku.buku_id");
			while(rs.next()) {
				model.addRow(new Object[] {
					rs.getString("id"),
					rs.getString("nama_peminjam"),
					rs.getString("judul_buku"),
					rs.getString("date_format(tanggal_pinjam, '%d-%m-%Y')"),
					rs.getString("date_format(tanggal_kembali, '%d-%m-%Y')"),
					rs.getString("denda"),
					rs.getString("biaya_sewa")
				});
			}
			
			stmt.close();
			conn.close();
			
			tableSewa.setModel(model);
			tableSewa.setAutoResizeMode(1);
		} catch(Exception e) {
			System.out.println("Koneksi gagal");
			System.out.println(e);
		}
	}
	
	public void getData(int buku_id, String judul) {
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			
			String query = "SELECT * FROM sewabuku WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(query);
			
			ps.setInt(1, buku_id);
			
			rs = ps.executeQuery();
			
			rs.next();
			
			txtPeminjam.setText(rs.getString("nama_peminjam"));
			boxJudulBuku.setSelectedItem(judul);
			
			stmt.close();
			conn.close();
		} catch(Exception e) {
			System.out.println("Koneksi gagal");
			System.out.println(e);
		}
	}
}
